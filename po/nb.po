# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-subtitles 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-07-09 23:40+0200\n"
"PO-Revision-Date: 2008-07-09 23:43+0200\n"
"Last-Translator: Kjartan Maraas <kmaraas@gnome.org>\n"
"Language-Team: Norwegian bokmål <kmaraas@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/Glade/AboutDialog.glade.h:1
msgid "About Gnome Subtitles"
msgstr "Om GNOME undertekst"

#: ../src/Glade/AboutDialog.glade.h:2
msgid "Video subtitling for the GNOME desktop"
msgstr ""

#: ../src/Glade/EncodingsDialog.glade.h:1
msgid "A_vailable:"
msgstr ""

#: ../src/Glade/EncodingsDialog.glade.h:2
msgid "Character codings"
msgstr "Tegnkoding"

#: ../src/Glade/EncodingsDialog.glade.h:3
msgid "Shown in menu:"
msgstr "Vist i meny:"

#: ../src/Glade/FileOpenDialog.glade.h:1
#: ../src/Glade/FileSaveAsDialog.glade.h:1
msgid "Character Coding:"
msgstr "Tegnkoding:"

#: ../src/Glade/FileOpenDialog.glade.h:2
#: ../src/GnomeSubtitles/Dialog/FileOpenDialog.cs:48
msgid "Open File"
msgstr "Åpne fil"

#: ../src/Glade/FileOpenDialog.glade.h:3
msgid "Video To Open:"
msgstr "Video som skal åpnes:"

#: ../src/Glade/FilePropertiesDialog.glade.h:1
msgid "<b>Character Coding:</b>"
msgstr "<b>Tegnkoding:</b>"

#: ../src/Glade/FilePropertiesDialog.glade.h:2
msgid "<b>Name:</b>"
msgstr "<b>Navn:</b>"

#: ../src/Glade/FilePropertiesDialog.glade.h:3
msgid "<b>Path:</b>"
msgstr "<b>Sti:</b>"

#: ../src/Glade/FilePropertiesDialog.glade.h:4
msgid "<b>Subtitle Format:</b>"
msgstr ""

#: ../src/Glade/FilePropertiesDialog.glade.h:5
msgid "<b>Timing Mode:</b>"
msgstr ""

#: ../src/Glade/FilePropertiesDialog.glade.h:6
msgid "File Properties"
msgstr ""

#: ../src/Glade/FileSaveAsDialog.glade.h:2
msgid "Newline Type:"
msgstr ""

#: ../src/Glade/FileSaveAsDialog.glade.h:3
#: ../src/GnomeSubtitles/Dialog/FileSaveAsDialog.cs:84
msgid "Save As"
msgstr ""

#: ../src/Glade/FileSaveAsDialog.glade.h:4
msgid "Select advanced options"
msgstr ""

#: ../src/Glade/FileSaveAsDialog.glade.h:5
msgid "Subtitle Format:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:1
msgid "Album:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:2
msgid "Artist:"
msgstr "Artist:"

#: ../src/Glade/HeadersDialog.glade.h:3
msgid ""
"Audio\n"
"Video\n"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:6
msgid "Author:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:7
msgid "By:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:8
msgid "CD Track:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:9
msgid "Collisions:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:10
msgid "Comment:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:11
msgid "Date:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:12
msgid "Delay:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:13
msgid "File Path:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:14
msgid "File:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:15
msgid "Font Color:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:16
msgid "Font Name:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:17
msgid "Font Size:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:18
msgid "Font Style:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:19
msgid "Frame rate:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:20
msgid "Headers"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:21
msgid "Karaoke Lyrics LRC"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:22
msgid "Karaoke Lyrics VKT"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:23
msgid "Note:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:24
msgid "Original Editing:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:25
msgid "Original Script Checking:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:26
msgid "Original Script:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:27
msgid "Original Timing:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:28
msgid "Original Translation:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:29
msgid "PlayDepth:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:30
msgid "PlayResX:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:31
msgid "PlayResY:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:32
msgid "Program:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:33
msgid "Script Updated By:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:34
msgid "Source:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:35
msgid "Timer:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:36
msgid "Title:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:37
msgid "Type:"
msgstr ""

#: ../src/Glade/HeadersDialog.glade.h:38
msgid "Version:"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:1
msgid "<b>Length</b>"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:2
msgid "<b>Time</b>"
msgstr ""

#. This is the duration of a subtitle.
#: ../src/Glade/MainWindow.glade.h:4
msgid "During:"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:5
msgid "Find Ne_xt"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:6
msgid "Find Pre_vious"
msgstr ""

#. This is the start time/frame of a subtitle.
#: ../src/Glade/MainWindow.glade.h:8
msgid "From:"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:9
msgid "Pr_eferences"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:10 ../src/Glade/SearchDialog.glade.h:1
#: ../src/GnomeSubtitles/Dialog/SearchDialog.cs:66
#: ../src/GnomeSubtitles/Dialog/SearchDialog.cs:116
msgid "Replace"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:11
msgid "Report a _Bug"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:12
msgid "Request a _Feature"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:13
msgid "Save _As"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:14
msgid "Seek _to Selection"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:15
msgid "Set Subtitle En_d"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:16
msgid "Set Subtitle _Start"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:17
msgid "Set Translatio_n Language"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:18
msgid "Set _Text Language"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:19
msgid "T_imings"
msgstr ""

#. This is the end time/frame of a subtitle.
#: ../src/Glade/MainWindow.glade.h:21
msgid "To:"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:22
msgid "Translatio_n"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:23
msgid "Vide_o"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:24
msgid "Video _Subtitles"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:25
msgid "_Adjust"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:26
msgid "_After"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:27
msgid "_Autocheck Spelling"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:28
msgid "_Before"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:29
msgid "_Bold"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:30
msgid "_Close"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:31
msgid "_Contents"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:32
msgid "_Delete Subtitles"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:33
msgid "_Edit"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:34
msgid "_File"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:35
msgid "_Format"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:36
msgid "_Frames"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:37
msgid "_Headers"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:38
msgid "_Help"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:39
msgid "_Input Frame Rate"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:40
msgid "_Insert Subtitle"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:41
msgid "_Italic"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:42
msgid "_New"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:43
msgid "_Open"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:44
msgid "_Play / Pause"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:45 ../src/Glade/SearchDialog.glade.h:7
msgid "_Replace"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:46
msgid "_Save"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:47
msgid "_Search"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:48
msgid "_Shift"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:49
msgid "_Text"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:50
msgid "_Times"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:51
msgid "_Tools"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:52
msgid "_Translation"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:53
msgid "_Underline"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:54
msgid "_Video"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:55
msgid "_Video Frame Rate"
msgstr ""

#: ../src/Glade/MainWindow.glade.h:56
msgid "_View"
msgstr "_Vis"

#: ../src/Glade/PreferencesDialog.glade.h:1
msgid "<b>Video</b>"
msgstr ""

#: ../src/Glade/PreferencesDialog.glade.h:2
msgid "Automatically choose video to open"
msgstr ""

#: ../src/Glade/PreferencesDialog.glade.h:3
msgid "Preferences"
msgstr "Brukervalg"

#: ../src/Glade/ReportBugWindow.glade.h:1
msgid ""
"An Error has occured.\n"
"\n"
"To report this bug, please <b>Open the Bugzilla</b> and paste the following "
"<b>log</b> into the bug description.\n"
"\n"
"If the bug is related to a <b>subtitle file</b>, please attach it if "
"possible."
msgstr ""

#: ../src/Glade/ReportBugWindow.glade.h:6
msgid "Open Bugzilla"
msgstr ""

#: ../src/Glade/SearchDialog.glade.h:2
msgid "Replace _All"
msgstr ""

#: ../src/Glade/SearchDialog.glade.h:3
msgid "Replace _with:"
msgstr ""

#: ../src/Glade/SearchDialog.glade.h:4
msgid "Search _backwards"
msgstr ""

#: ../src/Glade/SearchDialog.glade.h:5
msgid "Search using a Regular _Expression"
msgstr ""

#: ../src/Glade/SearchDialog.glade.h:6
msgid "_Match case"
msgstr ""

#: ../src/Glade/SearchDialog.glade.h:8
msgid "_Search for:"
msgstr ""

#: ../src/Glade/SearchDialog.glade.h:9
msgid "_Wrap around"
msgstr ""

#: ../src/Glade/SetLanguageDialog.glade.h:1
msgid "Set language"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:1
#: ../src/Glade/TimingsShiftDialog.glade.h:1
msgid "<b>Apply to</b>"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:2
msgid "<b>First Subtitle</b>"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:3
msgid "<b>Last Subtitle</b>"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:4
msgid "Adjust"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:5
msgid "Adjust Timings"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:6
msgid "All Subtitles"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:7
msgid "New Start:"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:8
msgid "No.:"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:9
msgid "Selected Subtitles"
msgstr ""

#: ../src/Glade/TimingsAdjustDialog.glade.h:10
msgid "Start Frame:"
msgstr ""

#: ../src/Glade/TimingsShiftDialog.glade.h:2
msgid "<b>Frames</b>"
msgstr ""

#: ../src/Glade/TimingsShiftDialog.glade.h:3
msgid "All subtitles"
msgstr ""

#. This means to apply the shift command from the selected subtitle to the first subtitle.
#: ../src/Glade/TimingsShiftDialog.glade.h:5
msgid "From first subtitle to selection"
msgstr ""

#. This means to apply the shift command from the selected subtitle to the last subtitle.
#: ../src/Glade/TimingsShiftDialog.glade.h:7
msgid "From selection to last subtitle"
msgstr ""

#: ../src/Glade/TimingsShiftDialog.glade.h:8
msgid "Selected subtitles"
msgstr ""

#: ../src/Glade/TimingsShiftDialog.glade.h:9
msgid "Shift"
msgstr ""

#: ../src/Glade/TimingsShiftDialog.glade.h:10
msgid "Shift Timings"
msgstr ""

#: ../src/Glade/VideoOpenDialog.glade.h:1
msgid "Open Video"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/AdjustTimingsCommand.cs:27
msgid "Adjusting timings"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ChangeFrameRateCommand.cs:47
msgid "Changing Input Frame Rate"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ChangeFrameRateCommand.cs:66
msgid "Changing Video Frame Rate"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ChangeStyleCommand.cs:56
msgid "Toggling Bold"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ChangeStyleCommand.cs:67
msgid "Toggling Italic"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ChangeStyleCommand.cs:78
msgid "Toggling Underline"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ChangeTimingCommand.cs:65
msgid "Editing From"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ChangeTimingCommand.cs:89
msgid "Editing To"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ChangeTimingCommand.cs:111
msgid "Editing During"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/CommandManager.cs:125
#: ../src/GnomeSubtitles/Ui/Menus.cs:96
msgid "Undo"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/CommandManager.cs:134
#: ../src/GnomeSubtitles/Ui/Menus.cs:106
msgid "Redo"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/DeleteSubtitlesCommand.cs:28
msgid "Deleting Subtitles"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/DeleteTextCommand.cs:27
#: ../src/GnomeSubtitles/Core/Command/InsertTextCommand.cs:27
msgid "Editing Text"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/DeleteTranslationCommand.cs:27
#: ../src/GnomeSubtitles/Core/Command/InsertTranslationCommand.cs:27
msgid "Editing Translation"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/InsertSubtitleCommand.cs:27
msgid "Inserting Subtitle"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ReplaceAllCommand.cs:30
msgid "Replacing All"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/ShiftTimingsCommand.cs:29
msgid "Shifting timings"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/VideoSetSubtitleTimingCommand.cs:26
msgid "Setting Subtitle Start"
msgstr ""

#: ../src/GnomeSubtitles/Core/Command/VideoSetSubtitleTimingCommand.cs:45
msgid "Setting Subtitle End"
msgstr ""

#: ../src/GnomeSubtitles/Core/Document.cs:199
msgid "Unsaved Translation"
msgstr ""

#. GEOSTD8, HZ not used
#: ../src/GnomeSubtitles/Core/Encodings.cs:33
#: ../src/GnomeSubtitles/Core/Encodings.cs:45
#: ../src/GnomeSubtitles/Core/Encodings.cs:76
#: ../src/GnomeSubtitles/Core/Encodings.cs:101
msgid "Western"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:34
#: ../src/GnomeSubtitles/Core/Encodings.cs:77
msgid "Central European"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:35
msgid "South European"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:36
#: ../src/GnomeSubtitles/Core/Encodings.cs:43
#: ../src/GnomeSubtitles/Core/Encodings.cs:106
msgid "Baltic"
msgstr ""

#. Added
#: ../src/GnomeSubtitles/Core/Encodings.cs:37
#: ../src/GnomeSubtitles/Core/Encodings.cs:78
#: ../src/GnomeSubtitles/Core/Encodings.cs:86
#: ../src/GnomeSubtitles/Core/Encodings.cs:92
#: ../src/GnomeSubtitles/Core/Encodings.cs:100
msgid "Cyrillic"
msgstr ""

#. Added
#: ../src/GnomeSubtitles/Core/Encodings.cs:38
#: ../src/GnomeSubtitles/Core/Encodings.cs:84
#: ../src/GnomeSubtitles/Core/Encodings.cs:105
msgid "Arabic"
msgstr ""

#. Added
#: ../src/GnomeSubtitles/Core/Encodings.cs:39
#: ../src/GnomeSubtitles/Core/Encodings.cs:87
#: ../src/GnomeSubtitles/Core/Encodings.cs:102
msgid "Greek"
msgstr ""

#. Added
#: ../src/GnomeSubtitles/Core/Encodings.cs:40
#: ../src/GnomeSubtitles/Core/Encodings.cs:82
#: ../src/GnomeSubtitles/Core/Encodings.cs:104
msgid "Hebrew"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:41
#: ../src/GnomeSubtitles/Core/Encodings.cs:79
#: ../src/GnomeSubtitles/Core/Encodings.cs:103
msgid "Turkish"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:42
#: ../src/GnomeSubtitles/Core/Encodings.cs:85
msgid "Nordic"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:44
msgid "Celtic"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:46
msgid "Romanian"
msgstr ""

#. ISO-8859-8-I not used
#. Added
#: ../src/GnomeSubtitles/Core/Encodings.cs:49
#: ../src/GnomeSubtitles/Core/Encodings.cs:50
#: ../src/GnomeSubtitles/Core/Encodings.cs:51
#: ../src/GnomeSubtitles/Core/Encodings.cs:52
#: ../src/GnomeSubtitles/Core/Encodings.cs:53
#: ../src/GnomeSubtitles/Core/Encodings.cs:54
#: ../src/GnomeSubtitles/Core/Encodings.cs:55
#: ../src/GnomeSubtitles/Core/Encodings.cs:56
msgid "Unicode"
msgstr ""

#. Added
#. UCS-2 and UCS-4 not used
#: ../src/GnomeSubtitles/Core/Encodings.cs:59
#: ../src/GnomeSubtitles/Core/Encodings.cs:60
msgid "Chinese Traditional"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:61
msgid "Cyrillic/Russian"
msgstr ""

#. ARMSCII-8 not used
#. Added
#. Added
#. ISO-IR-111, KOI8R, KOI8U not used
#: ../src/GnomeSubtitles/Core/Encodings.cs:64
#: ../src/GnomeSubtitles/Core/Encodings.cs:65
#: ../src/GnomeSubtitles/Core/Encodings.cs:89
#: ../src/GnomeSubtitles/Core/Encodings.cs:96
msgid "Japanese"
msgstr ""

#. EUC-JP-MS not used
#: ../src/GnomeSubtitles/Core/Encodings.cs:68
#: ../src/GnomeSubtitles/Core/Encodings.cs:90
#: ../src/GnomeSubtitles/Core/Encodings.cs:91
msgid "Korean"
msgstr ""

#. EUC-TW not used
#: ../src/GnomeSubtitles/Core/Encodings.cs:71
#: ../src/GnomeSubtitles/Core/Encodings.cs:72
#: ../src/GnomeSubtitles/Core/Encodings.cs:73
msgid "Chinese Simplified"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:80
msgid "Portuguese"
msgstr ""

#. Added
#: ../src/GnomeSubtitles/Core/Encodings.cs:81
msgid "Icelandic"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:83
msgid "French Canadian"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:93
msgid "Cyrillic/Ukrainian"
msgstr ""

#. TCVN, TIS-620, UHC, VISCII not used
#: ../src/GnomeSubtitles/Core/Encodings.cs:99
msgid "Central"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:107
msgid "Vietnamese"
msgstr ""

#: ../src/GnomeSubtitles/Core/Encodings.cs:118
msgid "Current Locale"
msgstr ""

#: ../src/GnomeSubtitles/Core/SpellLanguage.cs:30
#: ../src/GnomeSubtitles/Dialog/FilePropertiesDialog.cs:61
#: ../src/GnomeSubtitles/Dialog/FilePropertiesDialog.cs:68
#: ../src/GnomeSubtitles/Dialog/FilePropertiesDialog.cs:76
#: ../src/GnomeSubtitles/Dialog/FilePropertiesDialog.cs:80
#: ../src/GnomeSubtitles/Dialog/FilePropertiesDialog.cs:89
msgid "Unknown"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/EncodingsDialog.cs:88
msgid "Description"
msgstr "Beskrivelse"

#: ../src/GnomeSubtitles/Dialog/EncodingsDialog.cs:92
msgid "Encoding"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/ErrorDialog.cs:53
msgid ""
"An unknown error has occured. Please report a bug and include this error "
"name:"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileOpenDialog.cs:96
msgid "Auto Detected"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileOpenDialog.cs:143
msgid "None"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileOpenDialog.cs:209
#: ../src/GnomeSubtitles/Dialog/VideoOpenDialog.cs:60
msgid "All Files"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileOpenDialog.cs:216
msgid "All Subtitle Files"
msgstr ""

#. Strings
#: ../src/GnomeSubtitles/Dialog/FileOpenErrorDialog.cs:29
msgid "Could not open the file"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileOpenErrorDialog.cs:30
msgid "Open another file"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileOpenErrorDialog.cs:68
msgid "The file path appears to be invalid."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileSaveAsDialog.cs:86
msgid "Save Translation As"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileSaveAsDialog.cs:212
msgid "System Default"
msgstr ""

#. Strings
#: ../src/GnomeSubtitles/Dialog/FileSaveErrorDialog.cs:32
msgid "Could not save the file"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileSaveErrorDialog.cs:33
msgid "Save to another file"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileSaveErrorDialog.cs:59
#: ../src/GnomeSubtitles/Dialog/SubtitleFileOpenErrorDialog.cs:41
msgid "You have run out of memory. Please close some programs and try again."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileSaveErrorDialog.cs:61
#: ../src/GnomeSubtitles/Dialog/SubtitleFileOpenErrorDialog.cs:43
msgid "An I/O error has occured."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileSaveErrorDialog.cs:63
msgid "You do not have the permissions necessary to save the file."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileSaveErrorDialog.cs:65
#: ../src/GnomeSubtitles/Dialog/SubtitleFileOpenErrorDialog.cs:47
msgid "The specified file is invalid."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/FileTranslationOpenDialog.cs:27
msgid "Open Translation File"
msgstr ""

#. Strings
#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:34
msgid "If you don't save, all your changes will be permanently lost."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:85
#, csharp-format
msgid "Save the changes to subtitles \"{0}\" before creating new subtitles?"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:86
msgid "Create without Saving"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:96
#, csharp-format
msgid "Save the changes to translation \"{0}\" before creating new subtitles?"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:103
#, csharp-format
msgid ""
"Save the changes to translation \"{0}\" before creating a new translation?"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:113
#, csharp-format
msgid "Save the changes to subtitles \"{0}\" before opening?"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:114
msgid "Open without Saving"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:125
#, csharp-format
msgid "Save the changes to translation \"{0}\" before opening?"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:134
#, csharp-format
msgid "Save the changes to subtitles \"{0}\" before closing?"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:135
msgid "Close without Saving"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SaveConfirmationDialog.cs:145
#, csharp-format
msgid "Save the changes to translation \"{0}\" before closing?"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SearchDialog.cs:70
#: ../src/GnomeSubtitles/Dialog/SearchDialog.cs:120
msgid "Find"
msgstr ""

#. Strings
#: ../src/GnomeSubtitles/Dialog/SetLanguageDialog.cs:38
msgid "Set Text Language"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SetLanguageDialog.cs:39
msgid "Set Translation Language"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SetLanguageDialog.cs:40
msgid "Select the text _language of the current subtitles."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SetLanguageDialog.cs:41
msgid "Select the translation _language of the current subtitles."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SubtitleFileChooserDialog.cs:139
msgid "Add or Remove..."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SubtitleFileOpenErrorDialog.cs:37
msgid ""
"Unable to detect the subtitle format. Please check that the file type is "
"supported."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SubtitleFileOpenErrorDialog.cs:39
msgid ""
"The encoding used is not supported by your system. Please choose another "
"encoding."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SubtitleFileOpenErrorDialog.cs:45
msgid "You do not have the permissions necessary to open the file."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/SubtitleFileOpenErrorDialog.cs:49
msgid "The file could not be found."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/TimingsAdjustDialog.cs:68
msgid "Start Time:"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/TimingsShiftDialog.cs:75
#: ../src/GnomeSubtitles/Ui/VideoPreview/VideoPosition.cs:176
msgid "Time"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/TimingsShiftDialog.cs:75
msgid "Frames"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/VideoErrorDialog.cs:27
msgid "Could not continue the video playback"
msgstr ""

#: ../src/GnomeSubtitles/Dialog/VideoErrorDialog.cs:28
msgid "The following error has occurred: "
msgstr ""

#: ../src/GnomeSubtitles/Dialog/VideoFileOpenErrorDialog.cs:35
msgid "Please check that the video file is supported."
msgstr ""

#: ../src/GnomeSubtitles/Dialog/VideoOpenDialog.cs:66
msgid "All Video Files"
msgstr ""

#. To translators: this is the filename for new files (before being saved)
#: ../src/GnomeSubtitles/Ui/MainUi.cs:129
msgid "Unsaved Subtitles"
msgstr ""

#. TODO Tooltips has been deprecated
#. TODO Tooltips has been deprecated, scheduled for substitution when gtk# 2.12 is available in all major distros (use SVN revision 968)
#. Constant strings
#: ../src/GnomeSubtitles/Ui/Menus.cs:35
msgid "Video"
msgstr ""

#. To translators: OVR and INS correspond to the Overwrite and Insert text editing modes.
#: ../src/GnomeSubtitles/Ui/Status.cs:43
msgid "OVR"
msgstr ""

#: ../src/GnomeSubtitles/Ui/Status.cs:43
msgid "INS"
msgstr ""

#. To translators: Trans corresponds to Translation (used here to denote whether text or translation is being edited).
#: ../src/GnomeSubtitles/Ui/Status.cs:58
#: ../src/GnomeSubtitles/Ui/SubtitleView/SubtitleView.cs:231
#: ../src/GnomeSubtitles/Ui/View/SubtitleView.cs:233
msgid "Text"
msgstr ""

#: ../src/GnomeSubtitles/Ui/Status.cs:58
msgid "Trans"
msgstr ""

#. To translators: Ln corresponds to Line
#: ../src/GnomeSubtitles/Ui/Status.cs:60
msgid "Ln"
msgstr ""

#. To translators: Col corresponds to Column
#: ../src/GnomeSubtitles/Ui/Status.cs:62
msgid "Col"
msgstr ""

#: ../src/GnomeSubtitles/Ui/SubtitleView/SubtitleView.cs:223
#: ../src/GnomeSubtitles/Ui/View/SubtitleView.cs:225
msgid "No."
msgstr ""

#: ../src/GnomeSubtitles/Ui/SubtitleView/SubtitleView.cs:226
#: ../src/GnomeSubtitles/Ui/View/SubtitleView.cs:228
msgid "From"
msgstr ""

#: ../src/GnomeSubtitles/Ui/SubtitleView/SubtitleView.cs:227
#: ../src/GnomeSubtitles/Ui/View/SubtitleView.cs:229
msgid "To"
msgstr ""

#: ../src/GnomeSubtitles/Ui/SubtitleView/SubtitleView.cs:228
#: ../src/GnomeSubtitles/Ui/View/SubtitleView.cs:230
msgid "During"
msgstr ""

#: ../src/GnomeSubtitles/Ui/SubtitleView/SubtitleView.cs:232
#: ../src/GnomeSubtitles/Ui/View/SubtitleView.cs:234
msgid "Translation"
msgstr ""

#: ../src/GnomeSubtitles/Ui/VideoPreview/VideoPosition.cs:176
msgid "Frame"
msgstr ""
